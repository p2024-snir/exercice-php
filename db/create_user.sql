CREATE USER 'user'@'localhost' IDENTIFIED WITH caching_sha2_password BY 'Us3rUs€r';
GRANT USAGE ON *.* TO 'user'@'localhost';
GRANT SELECT, INSERT, UPDATE, DELETE ON  `cinema\_crud`.* TO 'user'@'localhost';
# Cinéma CRUD

## Déploiement

- Installer Composer https://getcomposer.org/download/
- Décompresser le zip dans votre répertoire de travail (ex. : ``c:/users/romain/source/repos/php/``)
- Créer un virtual host nommé ``cinema.local`` qui pointe vers ``c:/users/romain/source/repos/php/cinema_crud/src`` via l'assistant de Wamp
- Exécuter les scripts de BDD contenus dans ``db/``
    - D'abord ``create_base.sql``
    - Puis ``insert_data.sql``
    - Enfin ``create_constraints.sql``
- Créer l'utilisateur avec le script ``create_user.sql``
- Ouvrez votre répertoire de projet avec VS Code
- Ouvrez un terminal (CTRL + SHIFT + ù) et exécuter la commande ``composer install``à la racine du projet
- Rendez-vous à l'URL http://cinema.local

## Exercices

### Exercice 01

Énoncé disponible dès vendredi 5 Avril 2024 à 8h20.
